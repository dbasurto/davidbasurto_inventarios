package com.inventarios.service;

import com.inventarios.entity.ProductoEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 07 ene. 2022]
 **/
@Service
public interface ProductoService {

    List<ProductoEntity> getProducto();

    ProductoEntity getProductoCod(String cod);

    ProductoEntity getActualizarProducto(ProductoEntity producto);
}
