package com.inventarios.service;

import com.inventarios.entity.VentaEntity;
import com.inventarios.repository.VentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Service
public class VentaImpl implements VentaService {

    @Autowired
    private final VentaRepository ventaRepository;

    VentaImpl(VentaRepository ventaRepository) {
        this.ventaRepository = ventaRepository;
    }

    @Override
    public VentaEntity save(VentaEntity venta) {
        try {
            return ventaRepository.save(venta);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Integer getIdVenta() {
        try {
            return ventaRepository.getVentaMax().get(0).getId() + 1;
        } catch (Exception e) {
            return 1;
        }
    }

    @Override
    public List<Object[]> getReporte() {
        try {
            return ventaRepository.getReporte();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }
}
