package com.inventarios.service;

import com.inventarios.entity.ProductoEntity;
import okhttp3.OkHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * @version 1.0
 * @autor David Basurto [Date: 08 ene. 2022]
 **/
@Service
public class ConsomeApiImpl implements ConsomeApiService {

    Logger logger = Logger.getLogger(ConsomeApiImpl.class.getName());

    @Autowired
    private ProductoService productoService;

    public static okhttp3.Response peticionApi(String url) throws IOException {
        okhttp3.Response response = null;
        try {
            OkHttpClient httpClient = new OkHttpClient().newBuilder().connectTimeout(120000, TimeUnit.MILLISECONDS).readTimeout(45000, TimeUnit.MILLISECONDS).build();

            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .addHeader("X-HTTP-Method-Override", "GET")
                    .build();
            response = httpClient.newCall(request).execute();
        } catch (Exception e) {
            Logger.getLogger("Problemas en el servicio: {}", url);
        }
        return response;
    }

    public void cargarProducto(String url) {
        try {
            okhttp3.Response response = peticionApi(url);
            String result = response.body().string();
            var objectJson = new JSONObject(result);
            JSONArray jsonArray = objectJson.getJSONArray("prods");
            objectJson.getJSONArray("prods");
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject json = jsonArray.getJSONObject(i);
                    ProductoEntity producto = productoService.getProductoCod(json.getString("cod"));
                    if (Objects.isNull(producto)) {
                        producto = new ProductoEntity();
                        producto.setStock(json.getInt("stock"));
                    } else {
                        producto.setStock(producto.getStock() + json.getInt("stock"));
                    }
                    producto.setId(json.getInt("id"));
                    producto.setCod(json.getString("cod"));
                    producto.setName(json.getString("name"));
                    producto.setPrice(json.getBigDecimal("price"));
                    productoService.getActualizarProducto(producto);
                } catch (JSONException e) {
                    logger.severe("Error setear json. "+ e.getMessage());
                }
            }
        } catch (Exception e) {
            logger.severe("Error cargar producto json. "+e.getMessage());
        }
    }

    public ProductoEntity cargarProductoEspecifico(String url, String codProducto) {
        ProductoEntity producto = new ProductoEntity();
        try {
            okhttp3.Response response = peticionApi(url);
            String result = response.body().string();
            var objectJson = new JSONObject(result);
            JSONArray jsonArray = objectJson.getJSONArray("prods");
            objectJson.getJSONArray("prods");
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject json = jsonArray.getJSONObject(i);
                    if(codProducto.equals(json.getString("cod"))) {
                        producto = productoService.getProductoCod(json.getString("cod"));
                        if (Objects.isNull(producto)) {
                            producto = new ProductoEntity();
                            producto.setStock(json.getInt("stock"));
                        } else {
                            producto.setStock(producto.getStock() + json.getInt("stock"));
                        }
                        producto.setId(json.getInt("id"));
                        producto.setCod(json.getString("cod"));
                        producto.setName(json.getString("name"));
                        producto.setPrice(json.getBigDecimal("price"));
                        productoService.getActualizarProducto(producto);
                    }
                } catch (JSONException e) {
                    logger.severe("Error setear json. "+ e.getMessage());
                }
            }
        } catch (Exception e) {
            logger.severe("Error cargar producto json. "+e.getMessage());
        }
        return producto;
    }

}
