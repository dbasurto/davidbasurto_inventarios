package com.inventarios.controller;

import com.inventarios.entity.ProductoEntity;
import com.inventarios.service.ProductoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @version 1.0
 * @autor David Basurto [Date: 10 ene. 2022]
 **/
class ProductoControllerTest {

    @Mock
    private ProductoService productoService;

    @InjectMocks
    private ProductoController productoController;

    private ProductoEntity productoEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        productoEntity = new ProductoEntity();
        productoEntity.setId(1);
        productoEntity.setName("Producto 1");
        productoEntity.setCod("Prod-1");
    }

    @Test
    void producto() {
        when(productoService.getProducto()).thenReturn(Arrays.asList(productoEntity));
        assertNotNull(productoService.getProducto());
    }

    @Test
    void updateProducto() {
        when(productoService.getActualizarProducto(any(ProductoEntity.class))).thenReturn(productoEntity);
        assertNotNull(productoService.getActualizarProducto(productoEntity));
    }
}