package com.inventarios.repository;

import com.inventarios.entity.PedidoEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Repository
public interface PedidoRepository extends CrudRepository<PedidoEntity, Integer> {

    @Query("select p from PedidoEntity p order by p.id ")
    List<PedidoEntity> getPendidoMax();
}
