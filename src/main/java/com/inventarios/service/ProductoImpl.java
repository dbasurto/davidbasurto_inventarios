package com.inventarios.service;

import com.inventarios.entity.ProductoEntity;
import com.inventarios.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 08 ene. 2022]
 **/
@Service
public class ProductoImpl implements ProductoService {

    @Autowired
    private final ProductoRepository productoRepository;

    ProductoImpl(ProductoRepository productoRepository) {
        this.productoRepository = productoRepository;
    }

    @Override
    public List<ProductoEntity> getProducto() {
        try {
            return productoRepository.getProducto();
        }catch (Exception e){
            return new ArrayList<>();
        }
    }

    @Override
    public ProductoEntity getProductoCod(String cod) {
        try {
            return productoRepository.getProductoCod(cod);
        }catch (Exception e){
            return null;
        }
    }

    @Override
    public ProductoEntity getActualizarProducto(ProductoEntity producto) {
        try {
            return productoRepository.save(producto);
        }catch (Exception e){
            return null;
        }
    }
}
