package com.inventarios.repository;

import com.inventarios.entity.TransaccionEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Repository
public interface TransaccionRepository extends CrudRepository<TransaccionEntity, Integer> {

    @Query("select t from TransaccionEntity t order by t.id ")
    List<TransaccionEntity> getTransaccionMax();

    @Query("select ti.nombre,t.fecha,count(*) as cantidad from TransaccionEntity t join t.tiendaByIdTienda ti group by ti.nombre, t.fecha ")
    List<Object[]> getReporte();


    @Query("select t from TransaccionEntity t where fecha between :fechaInicio and :fechaFin ")
    List<TransaccionEntity> getTransaccionFecha(Date fechaInicio, Date fechaFin);

}
