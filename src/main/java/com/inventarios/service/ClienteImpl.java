package com.inventarios.service;

import com.inventarios.entity.ClienteEntity;
import com.inventarios.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 08 ene. 2022]
 **/
@Service
public class ClienteImpl implements ClienteService {

    @Autowired
    private final ClienteRepository clienteRepository;

    ClienteImpl(ClienteRepository clienteRepository) {
        this.clienteRepository = clienteRepository;
    }

    @Override
    public List<ClienteEntity> getCliente() {
        try {
            return clienteRepository.getCliente();
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public ClienteEntity getClienteByIdentificacion(String identificacion) {
        try {
            return clienteRepository.getClienteByIdentificacion(identificacion);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public ClienteEntity getClienteById(Integer id) {
        try {
            return clienteRepository.getClienteById(id);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public ClienteEntity save(ClienteEntity clienteEntity) {
        try {
            return clienteRepository.save(clienteEntity);
        } catch (Exception e) {
            return null;
        }
    }
    
    @Override
    public void delete(ClienteEntity clienteEntity) {
        try {
            clienteRepository.delete(clienteEntity);
        } catch (Exception e) {

        }
    }
    
    @Override
    public Integer idCliente() {
        try {
            return clienteRepository.getClienteMax().get(0).getId() + 1;
        } catch (Exception e) {
            return 1;
        }
    }
}
