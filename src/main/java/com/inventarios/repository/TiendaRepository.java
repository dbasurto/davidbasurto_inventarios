package com.inventarios.repository;

import com.inventarios.entity.TiendaEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Repository
public interface TiendaRepository extends CrudRepository<TiendaEntity, Integer> {

    @Query("select t from TiendaEntity t where t.codigo=:codigo")
    TiendaEntity getTiendaByCodigo(String codigo);

}
