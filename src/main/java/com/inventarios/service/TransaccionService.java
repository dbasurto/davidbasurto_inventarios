package com.inventarios.service;

import com.inventarios.entity.TransaccionEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Service
public interface TransaccionService {

    TransaccionEntity save(Integer idCliente, Integer idProducto, Integer cantidad, Integer idTienda);

    List<Object[]> getReporte();

    List<TransaccionEntity> getTransaccionFecha(Date fechaInicio, Date fechaFin);

}
