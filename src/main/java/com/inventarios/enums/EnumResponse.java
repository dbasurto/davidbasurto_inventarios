package com.inventarios.enums;

/**
 * @version 1.0
 * @autor David Basurto [Date: 07 ene. 2022]
 **/
public enum EnumResponse {


    OK("1", "OK"),
    ERROR("0", "ERROR"),
    STOCK("0", "Stock debe ser mayor a 0!"),
    CANTIDAD("0", "Cantidad venta debe ser mayor a 0!"),
    NO_PRODUCTO("0", "No existe producto!"),
    NO_TIENDA("0", "No existe tienda!"),
    NO_STOKC("0", "No existe stock del producto!"),
    ACTUALIZAR_PRODUCTO("1", "Se actualizó el producto correctamente!"),
    NO_CLIENTE("0", "No existe cliente con identificación ingresada!"),
    NO_CLIENTE_ID("0", "No existe cliente con id ingresado!"),
    CLIENTE_UDP("1", "Cliente actualizado con éxito!"),
    CLIENTE_CRT("1", "Cliente creado con éxito!"),
    CLIENTE_DLT("1", "Cliente eliminado con éxito!"),
    VENTA("1", "Venta realizada con éxito!"),
    PEDIDO("1", "Pedido realizado con éxito!"),
    RANGO_FECHA_INCORRECTO("0", "Rango de fechas incorrecto!"),
    UNIDADES_NO_DISPONIBLES("0", "Unidades no disponibles (> 10)");

    private EnumResponse(String code, String value) {
        this.code=code;
        this.value=value;
    }

    private String code;
    private String value;

    public String code() {
        return code;
    }
    public String value() {
        return value;
    }
}
