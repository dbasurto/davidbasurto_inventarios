package com.inventarios;

import com.inventarios.service.ConsomeApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.stereotype.Service;

@SpringBootApplication
public class DavidBasurtoInventariosApplication extends SpringBootServletInitializer {

    private static final Class<DavidBasurtoInventariosApplication> applicationClass = DavidBasurtoInventariosApplication.class;

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(DavidBasurtoInventariosApplication.class, args);
        CargaService service = applicationContext.getBean(CargaService.class);
        service.carga(args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(applicationClass);
    }

    @Configuration
    @EnableWebSecurity
    @EnableGlobalMethodSecurity(prePostEnabled = true)
    public class SecurityConfig extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity security) throws Exception {
            security.csrf().disable().authorizeRequests().anyRequest().permitAll();
        }
    }


}

@Service
class CargaService {
    @Autowired
    private ConsomeApiService consomeApiService;

    public void carga(String[] args) {
        consomeApiService.cargarProducto("https://mocki.io/v1/50dceff9-461a-4366-96a3-cf9f4983aad2");
    }
}
