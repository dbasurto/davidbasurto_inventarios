package com.inventarios.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Entity
@Table(name = "venta")
public class VentaEntity {
    private Integer id;
    private Integer idTienda;
    private Integer idProducto;
    private Integer idCliente;
    private Integer cantidad;
    private BigDecimal precio;
    private Date fecha;
    private TiendaEntity tiendaByIdTienda;
    private ProductoEntity productoByIdProducto;
    private ClienteEntity clienteByIdCliente;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_tienda", nullable = true)
    public Integer getIdTienda() {
        return idTienda;
    }

    public void setIdTienda(Integer idTienda) {
        this.idTienda = idTienda;
    }

    @Basic
    @Column(name = "id_producto", nullable = true)
    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    @Basic
    @Column(name = "id_cliente", nullable = true)
    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    @Basic
    @Column(name = "cantidad", nullable = true)
    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Basic
    @Column(name = "precio", nullable = true)
    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "fecha", nullable = true)
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VentaEntity that = (VentaEntity) o;
        return id == that.id && Objects.equals(idTienda, that.idTienda) && Objects.equals(idProducto, that.idProducto)  && Objects.equals(cantidad, that.cantidad) && Objects.equals(precio, that.precio) && Objects.equals(fecha, that.fecha);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idTienda, idProducto, cantidad, precio, fecha);
    }

    @Access(AccessType.PROPERTY)
    @ManyToOne
    @JoinColumn(name = "id_tienda", referencedColumnName = "id", insertable = false, updatable = false)
    public TiendaEntity getTiendaByIdTienda() {
        return tiendaByIdTienda;
    }

    public void setTiendaByIdTienda(TiendaEntity tiendaByIdTienda) {
        this.tiendaByIdTienda = tiendaByIdTienda;
    }

    @Access(AccessType.PROPERTY)
    @ManyToOne
    @JoinColumn(name = "id_producto", referencedColumnName = "id", insertable = false, updatable = false)
    public ProductoEntity getProductoByIdProducto() {
        return productoByIdProducto;
    }

    public void setProductoByIdProducto(ProductoEntity productoByIdProducto) {
        this.productoByIdProducto = productoByIdProducto;
    }

    @Access(AccessType.PROPERTY)
    @ManyToOne
    @JoinColumn(name = "id_cliente", referencedColumnName = "id", insertable = false, updatable = false)
    public ClienteEntity getClienteByIdCliente() {
        return clienteByIdCliente;
    }

    public void setClienteByIdCliente(ClienteEntity clienteByIdCliente) {
        this.clienteByIdCliente = clienteByIdCliente;
    }
}
