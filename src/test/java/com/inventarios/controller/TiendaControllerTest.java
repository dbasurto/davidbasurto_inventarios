package com.inventarios.controller;

import com.inventarios.entity.VentaEntity;
import com.inventarios.service.VentaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @version 1.0
 * @autor David Basurto [Date: 10 ene. 2022]
 **/
class TiendaControllerTest {

    @Mock
    private VentaService ventaService;

    private VentaEntity ventaEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        ventaEntity = new VentaEntity();
        ventaEntity.setId(1);
        ventaEntity.setIdCliente(1);
        ventaEntity.setIdProducto(1);
        ventaEntity.setIdTienda(1);
        ventaEntity.setFecha(new Date());
    }

    @Test
    void getVenta() {
        when(ventaService.save(any(VentaEntity.class))).thenReturn(ventaEntity);
        assertNotNull(ventaService.save(ventaEntity));
    }

}