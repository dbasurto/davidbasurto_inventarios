package com.inventarios.service;

import com.inventarios.entity.ProductoEntity;
import org.springframework.stereotype.Service;

/**
 * @version 1.0
 * @autor David Basurto [Date: 08 ene. 2022]
 **/
@Service
public interface ConsomeApiService {

    void cargarProducto(String url);

    ProductoEntity cargarProductoEspecifico(String url, String codProducto);
}
