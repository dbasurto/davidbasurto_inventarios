package com.inventarios.exception;

import java.util.HashMap;

/**
 * @version 1.0
 * @autor David Basurto [Date: 08 ene. 2022]
 **/
public class InventarioException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public InventarioException() {
        super();
    }

    public InventarioException(String message) {
        super(message);
    }

    public InventarioException(Throwable cause) {
        super(cause);
    }

    public InventarioException(String message, Throwable cause) {
        super(message, cause);
    }
}
