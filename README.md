# Ejercicio práctico Java BackEnd Developer

## Iniciar
* Ejecutar script schema-inventario.sql ubicado en resources para generar bases de datos en postgres de nombre inventario.
* Cambiar las credenciales de la Base de Datos

##Script Base de Datos
Ruta: /src/main/resources/schema-inventario.sql

##Curl de los End Points
#####Cargar Producto
curl --request GET \
--url http://localhost:8080/producto/allProduct \
--cookie JSESSIONID=3304CF1079A24AA0F4EFD4115F5F762F

####Actualizar producto
curl --request POST \
--url http://localhost:8080/producto/updateProduct \
--header 'Content-Type: application/x-www-form-urlencoded' \
--cookie JSESSIONID=3304CF1079A24AA0F4EFD4115F5F762F \
--data codProduct=prod-1 \
--data stock=0

####Cargar Todos los Cliente
curl --request GET \
--url http://localhost:8080/cliente/allClient \
--cookie JSESSIONID=3304CF1079A24AA0F4EFD4115F5F762F

####Cargar Cliente por Identificación
curl --request GET \
--url http://localhost:8080/cliente/getClient/1726541254 \
--cookie JSESSIONID=3304CF1079A24AA0F4EFD4115F5F762F

####Crear Cliente
curl --request POST \
--url http://localhost:8080/cliente/createClient \
--header 'Content-Type: application/x-www-form-urlencoded' \
--cookie JSESSIONID=3304CF1079A24AA0F4EFD4115F5F762F \
--data 'identificación=1726541252' \
--data 'nombre=María Vera'

####Actualizar Cliente
curl --request POST \
--url http://localhost:8080/cliente/updateClient \
--header 'Content-Type: application/x-www-form-urlencoded' \
--cookie JSESSIONID=3304CF1079A24AA0F4EFD4115F5F762F \
--data id=1 \
--data 'identificación=1726541253' \
--data 'nombre=Camilo Castro'

####Eliminar Cliente
curl --request GET \
--url http://localhost:8080/cliente/deleteClient/2 \
--header 'Content-Type: application/x-www-form-urlencoded' \
--cookie JSESSIONID=3304CF1079A24AA0F4EFD4115F5F762F

####Venta en tienda
curl --request POST \
--url http://localhost:8080/tienda/venta \
--header 'Content-Type: application/x-www-form-urlencoded' \
--cookie JSESSIONID=3304CF1079A24AA0F4EFD4115F5F762F \
--data codProducto=prod-1 \
--data codTienda=tie-1 \
--data cantidad=1 \
--data codCliente=1

####Reporte 1
curl --request GET \
--url http://localhost:8080/reporte/transacciones \
--header 'Content-Type: application/x-www-form-urlencoded' \
--cookie JSESSIONID=3304CF1079A24AA0F4EFD4115F5F762F

####Reporte 1
curl --request GET \
--url http://localhost:8080/reporte/transacciones \
--header 'Content-Type: application/x-www-form-urlencoded' \
--cookie JSESSIONID=3304CF1079A24AA0F4EFD4115F5F762F

####Reporte Genera Csv y Descarga
curl --request GET \
--url http://localhost:8080/reporte/download/3/09-01-2022/09-01-2022/transacciones.csv \
--header 'Content-Type: application/x-www-form-urlencoded' \
--cookie JSESSIONID=3304CF1079A24AA0F4EFD4115F5F762F

####Genera Pedido
curl --request POST \
--url http://localhost:8080/pedido/pedido \
--header 'Content-Type: application/x-www-form-urlencoded' \
--cookie JSESSIONID=3304CF1079A24AA0F4EFD4115F5F762F \
--data codTienda=tie-1 \
--data codProducto=prod-1 \
--data cantidad=15 \
--data codCliente=1
