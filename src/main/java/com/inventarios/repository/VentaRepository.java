package com.inventarios.repository;

import com.inventarios.entity.VentaEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Repository
public interface VentaRepository extends CrudRepository<VentaEntity, Integer> {

    @Query("select v from VentaEntity v order by v.id ")
    List<VentaEntity> getVentaMax();

    @Query("select ti.nombre,p.name, sum(v.cantidad * v.precio) as monto from VentaEntity v " +
            "join v.tiendaByIdTienda ti " +
            "join v.productoByIdProducto p " +
            "group by ti.nombre,p.name")
    List<Object[]> getReporte();
}
