package com.inventarios.Utils;

import com.inventarios.entity.TransaccionEntity;
import org.springframework.http.ResponseEntity;

import java.io.PrintWriter;
import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
public class CsvUtils {
    public static ResponseEntity<Object> downloadCsv(PrintWriter writer, List<TransaccionEntity> lstTransacciones) {
        writer.write("ID, Tienda, Producto, Cliente, Cantidad, Fecha \n");
        for (TransaccionEntity t : lstTransacciones) {
            writer.write(t.getId() + "," + t.getTiendaByIdTienda().getNombre() + "," + t.getProductoByIdProducto().getName()
                    + "," + t.getClienteByIdCliente().getNombre() + "," + t.getCantidad() + "," + t.getFecha() + "\n");
        }
        return null;
    }
}
