package com.inventarios.service;

import com.inventarios.entity.ClienteEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 08 ene. 2022]
 **/
@Service
public interface ClienteService {

    List<ClienteEntity> getCliente();

    ClienteEntity getClienteByIdentificacion(String identificacion);

    ClienteEntity getClienteById(Integer id);

    ClienteEntity save(ClienteEntity clienteEntity);

    void delete(ClienteEntity clienteEntity);

    Integer idCliente();
}
