package com.inventarios.controller;

import com.inventarios.Utils.CsvUtils;
import com.inventarios.entity.ClienteEntity;
import com.inventarios.entity.TransaccionEntity;
import com.inventarios.enums.EnumResponse;
import com.inventarios.exception.InventarioException;
import com.inventarios.model.ResponseModel;
import com.inventarios.service.ClienteService;
import com.inventarios.service.TransaccionService;
import com.inventarios.service.VentaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;


/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@RestController
@RequestMapping("/reporte")
public class ReporteController {

    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

    Logger logger = Logger.getLogger(ReporteController.class.getName());
    @Autowired
    private TransaccionService transaccionService;

    @Autowired
    private VentaService ventaService;

    @Autowired
    private ClienteService clienteService;

    @GetMapping(value = "/test")
    public ResponseEntity<ResponseModel> test() {
        var response = new ResponseModel(EnumResponse.OK.code());
        try {
            response.setMessage("Test...");
        } catch (Exception e) {
            response.setCode(EnumResponse.ERROR.code());
            response.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/transacciones")
    public ResponseEntity<List<HashMap<String, Object>>> obtenerTransacciones() {
        HashMap<String, Object> respuesta = new HashMap<>();
        List<HashMap<String, Object>> lstRespuesta = new ArrayList<>();
        try {
            List<Object[]> lstReporte = transaccionService.getReporte();
            if (!lstReporte.isEmpty()) {
                for (Object[] o : lstReporte) {
                    respuesta = new HashMap<>();
                    respuesta.put("Cantidad", o[2]);
                    respuesta.put("Tienda", o[0]);
                    respuesta.put("Fecha", o[1]);
                    lstRespuesta.add(respuesta);
                }
            }
        } catch (Exception e) {
            respuesta.put(EnumResponse.ERROR.code(), EnumResponse.ERROR.value());
        }
        return ResponseEntity.ok().body(lstRespuesta);
    }

    @GetMapping(value = "/vendido")
    public ResponseEntity<List<HashMap<String, Object>>> obtenerVendido() {
        HashMap<String, Object> respuesta = new HashMap<>();
        List<HashMap<String, Object>> lstRespuesta = new ArrayList<>();
        try {
            List<Object[]> lstReporte = ventaService.getReporte();
            if (!lstReporte.isEmpty()) {
                for (Object[] o : lstReporte) {
                    respuesta = new HashMap<>();
                    respuesta.put("Monto", o[2]);
                    respuesta.put("Tienda", o[0]);
                    respuesta.put("Producto", o[1]);
                    lstRespuesta.add(respuesta);
                }
            }
        } catch (Exception e) {
            respuesta.put(EnumResponse.ERROR.code(), EnumResponse.ERROR.value());
        }
        return ResponseEntity.ok().body(lstRespuesta);
    }


    @GetMapping("/download/{idCliente}/{fechaInicio}/{fechaFin}/transacciones.csv")
    public ResponseEntity<Object> downloadCsv(HttpServletResponse response,
                                              @PathVariable("idCliente") final Integer idCliente,
                                              @PathVariable("fechaInicio") final String fechaInicio,
                                              @PathVariable("fechaFin") final String fechaFin) throws IOException {
        String message = "";
        List<TransaccionEntity> lstTransacciones = new ArrayList<>();
        try {
            ClienteEntity cliente = clienteService.getClienteById(idCliente);
            Date fInicio = dateFormat.parse(fechaInicio);
            Date fFin = dateFormat.parse(fechaFin);
            if (Objects.isNull(cliente)) {
                message = EnumResponse.NO_CLIENTE_ID.value();
                throw new InventarioException(EnumResponse.NO_CLIENTE_ID.value());
            }
            if (fInicio.getTime() > fFin.getTime()) {
                message = EnumResponse.RANGO_FECHA_INCORRECTO.value();
                throw new InventarioException(EnumResponse.RANGO_FECHA_INCORRECTO.value());
            }
            lstTransacciones = transaccionService.getTransaccionFecha(fInicio, fFin);
        } catch (Exception e) {
            logger.severe(message);
        }
        if (message.equalsIgnoreCase("")) {
            return ResponseEntity.ok().body(downloadCsv(response, lstTransacciones));
        } else {
            HashMap<String, Object> respuesta = new HashMap<>();
            respuesta.put(EnumResponse.ERROR.code(), message);
            return ResponseEntity.ok().body(respuesta);
        }
    }

    public ResponseEntity<Object> downloadCsv(HttpServletResponse response, List<TransaccionEntity> lstTransacciones) throws IOException {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment; file=transacciones.csv");

        return CsvUtils.downloadCsv(response.getWriter(), lstTransacciones);
    }

}
