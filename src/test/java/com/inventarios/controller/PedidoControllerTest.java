package com.inventarios.controller;

import com.inventarios.entity.PedidoEntity;
import com.inventarios.service.PedidoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @version 1.0
 * @autor David Basurto [Date: 10 ene. 2022]
 **/
class PedidoControllerTest {

    @Mock
    private PedidoService pedidoService;

    private PedidoEntity pedidoEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        pedidoEntity = new PedidoEntity();
        pedidoEntity.setId(1);
        pedidoEntity.setIdCliente(1);
        pedidoEntity.setIdProducto(1);
        pedidoEntity.setIdTienda(1);
        pedidoEntity.setCantidad(1);
    }

    @Test
    void getPedido() {
        when(pedidoService.save(any(PedidoEntity.class))).thenReturn(pedidoEntity);
        assertNotNull(pedidoService.save(pedidoEntity));
    }
}