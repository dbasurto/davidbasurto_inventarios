package com.inventarios.controller;

import com.inventarios.entity.ClienteEntity;
import com.inventarios.service.ClienteService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * @version 1.0
 * @autor David Basurto [Date: 10 ene. 2022]
 **/
class ClienteControllerTest {

    @Mock
    private ClienteService clienteService;

    @InjectMocks
    private ClienteController clienteController;

    private ClienteEntity clienteEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        clienteEntity = new ClienteEntity();
        clienteEntity.setId(1);
        clienteEntity.setNombre("cliente 1");
        clienteEntity.setIdentificacion("1726521452");
    }

    @Test
    void getAllCliente() {
        when(clienteService.getCliente()).thenReturn(Arrays.asList(clienteEntity));
        assertNotNull(clienteService.getCliente());
    }

}