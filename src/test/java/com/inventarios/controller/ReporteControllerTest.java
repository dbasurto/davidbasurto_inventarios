package com.inventarios.controller;

import com.inventarios.entity.TransaccionEntity;
import com.inventarios.service.TransaccionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * @version 1.0
 * @autor David Basurto [Date: 10 ene. 2022]
 **/
class ReporteControllerTest {

    @Mock
    private TransaccionService transaccionService;

    private Object[] object;

    private TransaccionEntity transaccionEntity;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        transaccionEntity = new TransaccionEntity();
        transaccionEntity.setId(1);
        transaccionEntity.setIdCliente(1);
        transaccionEntity.setIdProducto(1);
        transaccionEntity.setCantidad(1);
        transaccionEntity.setIdTienda(1);
    }

    @Test
    void test1() {
    }

    @Test
    void obtenerTransacciones() {
        when(transaccionService.getReporte()).thenReturn(Collections.singletonList(object));
        assertNotNull(transaccionService.getReporte());
    }

    @Test
    void obtenerVendido() {
        when(transaccionService.getTransaccionFecha(new Date(""), new Date(""))).thenReturn(Arrays.asList(transaccionEntity));
        assertNotNull(transaccionService.getReporte());
    }
}