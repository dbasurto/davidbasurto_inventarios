package com.inventarios.repository;

import com.inventarios.entity.ClienteEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 08 ene. 2022]
 **/
public interface ClienteRepository extends CrudRepository<ClienteEntity, Integer> {

    @Query("select c from ClienteEntity c")
    List<ClienteEntity> getCliente();

    @Query("select c from ClienteEntity c where c.identificacion=:identificacion")
    ClienteEntity getClienteByIdentificacion(String identificacion);

    @Query("select c from ClienteEntity c where c.id=:id")
    ClienteEntity getClienteById(Integer id);

    @Query("select c from ClienteEntity c order by c.id ")
    List<ClienteEntity> getClienteMax();

}
