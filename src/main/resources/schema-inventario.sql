--
-- PostgreSQL database dump
--

-- Dumped from database version 10.11
-- Dumped by pg_dump version 14.1

-- Started on 2022-01-10 11:36:59

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

DROP DATABASE inventario;
--
-- TOC entry 3719 (class 1262 OID 3980731)
-- Name: inventario; Type: DATABASE; Schema: -; Owner: postgres
--

CREATE DATABASE inventario WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.UTF-8';


ALTER DATABASE inventario OWNER TO postgres;

\connect inventario

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2 (class 3079 OID 21587)
-- Name: pg_buffercache; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_buffercache WITH SCHEMA public;


--
-- TOC entry 3720 (class 0 OID 0)
-- Dependencies: 2
-- Name: EXTENSION pg_buffercache; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_buffercache IS 'examine the shared buffer cache';


--
-- TOC entry 3 (class 3079 OID 16384)
-- Name: pg_stat_statements; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_stat_statements WITH SCHEMA public;


--
-- TOC entry 3721 (class 0 OID 0)
-- Dependencies: 3
-- Name: EXTENSION pg_stat_statements; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_stat_statements IS 'track execution statistics of all SQL statements executed';


SET default_tablespace = '';

--
-- TOC entry 202 (class 1259 OID 3980742)
-- Name: cliente; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cliente (
    id bigint NOT NULL,
    identificacion character varying(15),
    nombre character varying(50)
);


ALTER TABLE public.cliente OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 3980747)
-- Name: pedido; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pedido (
    id bigint NOT NULL,
    id_cliente integer,
    id_tienda integer,
    id_producto integer,
    cantidad integer
);


ALTER TABLE public.pedido OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 3980732)
-- Name: producto; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.producto (
    id bigint NOT NULL,
    cod character varying(25),
    name character varying(50),
    price money,
    stock integer
);


ALTER TABLE public.producto OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 3980737)
-- Name: tienda; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tienda (
    id bigint NOT NULL,
    codigo character varying(25),
    nombre character varying(50)
);


ALTER TABLE public.tienda OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 3991618)
-- Name: transaccion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transaccion (
    id bigint NOT NULL,
    id_tienda integer,
    id_producto integer,
    id_cliente integer,
    cantidad integer,
    fecha timestamp without time zone
);


ALTER TABLE public.transaccion OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 3991598)
-- Name: venta; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.venta (
    id bigint NOT NULL,
    id_tienda integer,
    id_producto integer,
    id_cliente integer,
    cantidad integer,
    precio money,
    fecha date
);


ALTER TABLE public.venta OWNER TO postgres;

--
-- TOC entry 3575 (class 2606 OID 3980746)
-- Name: cliente pk_cliente; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cliente
    ADD CONSTRAINT pk_cliente PRIMARY KEY (id);


--
-- TOC entry 3577 (class 2606 OID 3980751)
-- Name: pedido pk_pedido; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pedido
    ADD CONSTRAINT pk_pedido PRIMARY KEY (id);


--
-- TOC entry 3571 (class 2606 OID 3980736)
-- Name: producto pk_producto; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.producto
    ADD CONSTRAINT pk_producto PRIMARY KEY (id);


--
-- TOC entry 3573 (class 2606 OID 3980741)
-- Name: tienda pk_tienda; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tienda
    ADD CONSTRAINT pk_tienda PRIMARY KEY (id);


--
-- TOC entry 3581 (class 2606 OID 3991622)
-- Name: transaccion pk_transaccion; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaccion
    ADD CONSTRAINT pk_transaccion PRIMARY KEY (id);


--
-- TOC entry 3579 (class 2606 OID 3991602)
-- Name: venta pk_venta; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.venta
    ADD CONSTRAINT pk_venta PRIMARY KEY (id);


--
-- TOC entry 3582 (class 2606 OID 3980752)
-- Name: pedido fk_cliente; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pedido
    ADD CONSTRAINT fk_cliente FOREIGN KEY (id_cliente) REFERENCES public.cliente(id);


--
-- TOC entry 3590 (class 2606 OID 3991633)
-- Name: transaccion fk_cliente_transaccion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaccion
    ADD CONSTRAINT fk_cliente_transaccion FOREIGN KEY (id_cliente) REFERENCES public.cliente(id);


--
-- TOC entry 3584 (class 2606 OID 3980762)
-- Name: pedido fk_producto; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pedido
    ADD CONSTRAINT fk_producto FOREIGN KEY (id_producto) REFERENCES public.producto(id);


--
-- TOC entry 3589 (class 2606 OID 3991628)
-- Name: transaccion fk_producto_transaccion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaccion
    ADD CONSTRAINT fk_producto_transaccion FOREIGN KEY (id_producto) REFERENCES public.producto(id);


--
-- TOC entry 3583 (class 2606 OID 3980757)
-- Name: pedido fk_tienda; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pedido
    ADD CONSTRAINT fk_tienda FOREIGN KEY (id_tienda) REFERENCES public.tienda(id);


--
-- TOC entry 3588 (class 2606 OID 3991623)
-- Name: transaccion fk_tienda_transaccion; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transaccion
    ADD CONSTRAINT fk_tienda_transaccion FOREIGN KEY (id_tienda) REFERENCES public.tienda(id);


--
-- TOC entry 3585 (class 2606 OID 3991603)
-- Name: venta fk_venta_cliente; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.venta
    ADD CONSTRAINT fk_venta_cliente FOREIGN KEY (id_cliente) REFERENCES public.cliente(id);


--
-- TOC entry 3586 (class 2606 OID 3991608)
-- Name: venta fk_venta_producto; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.venta
    ADD CONSTRAINT fk_venta_producto FOREIGN KEY (id_producto) REFERENCES public.producto(id);


--
-- TOC entry 3587 (class 2606 OID 3991613)
-- Name: venta fk_venta_tienda; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.venta
    ADD CONSTRAINT fk_venta_tienda FOREIGN KEY (id_tienda) REFERENCES public.tienda(id);


-- Completed on 2022-01-10 11:37:00

--
-- PostgreSQL database dump complete
--

