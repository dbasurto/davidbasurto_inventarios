package com.inventarios.repository;


import com.inventarios.entity.ProductoEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 07 ene. 2022]
 **/
@Repository
public interface ProductoRepository extends CrudRepository<ProductoEntity, Integer> {

    @Query("select p from ProductoEntity p")
    List<ProductoEntity> getProducto();

    @Query("select p from ProductoEntity p where cod=:cod")
    ProductoEntity getProductoCod(String cod);
}
