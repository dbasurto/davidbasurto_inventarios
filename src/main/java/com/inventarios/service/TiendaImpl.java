package com.inventarios.service;

import com.inventarios.entity.TiendaEntity;
import com.inventarios.repository.TiendaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Service
public class TiendaImpl implements TiendaService {

    @Autowired
    private final TiendaRepository tiendaRepository;

    TiendaImpl(TiendaRepository tiendaRepository) {
        this.tiendaRepository = tiendaRepository;
    }


    @Override
    public TiendaEntity getTiendaByCodigo(String codigo) {
        try {
            return tiendaRepository.getTiendaByCodigo(codigo);
        } catch (Exception e) {
            return null;
        }
    }

}
