package com.inventarios.controller;

import com.inventarios.entity.ClienteEntity;
import com.inventarios.entity.ProductoEntity;
import com.inventarios.entity.TiendaEntity;
import com.inventarios.entity.VentaEntity;
import com.inventarios.enums.EnumResponse;
import com.inventarios.exception.InventarioException;
import com.inventarios.model.ResponseModel;
import com.inventarios.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@RestController
@RequestMapping("/tienda")
public class TiendaController {

    @Autowired
    private ProductoService productoService;

    @Autowired
    private TiendaService tiendaService;

    @Autowired
    private VentaService ventaService;

    @Autowired
    private TransaccionService transaccionService;

    @Autowired
    private ClienteService clienteService;

    @GetMapping(value = "/test")
    public ResponseEntity<ResponseModel> test() {
        var response = new ResponseModel(EnumResponse.OK.code());
        try {
            response.setMessage("Test...");
        } catch (Exception e) {
            response.setCode(EnumResponse.ERROR.code());
            response.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/venta")
    public ResponseEntity<Map<String, Object>> getVenta(@RequestParam("codTienda") String codTienda,
                                                        @RequestParam("codProducto") String codProducto,
                                                        @RequestParam("cantidad") Integer cantidad,
                                                        @RequestParam("codCliente") Integer codCliente) {
        HashMap<String, Object> respuesta = new HashMap<>();
        String message = "";
        try {
            ProductoEntity producto = productoService.getProductoCod(codProducto);
            if (Objects.isNull(producto)) {
                message = EnumResponse.NO_PRODUCTO.value();
                throw new InventarioException(EnumResponse.NO_PRODUCTO.value());
            }
            if (producto.getStock() <= 0) {
                message = EnumResponse.NO_STOKC.value();
                throw new InventarioException(EnumResponse.NO_STOKC.value());
            }
            TiendaEntity tienda = tiendaService.getTiendaByCodigo(codTienda);
            if (Objects.isNull(tienda)) {
                message = EnumResponse.NO_TIENDA.value();
                throw new InventarioException(EnumResponse.NO_TIENDA.value());
            }
            if (cantidad <= 0) {
                message = EnumResponse.CANTIDAD.value();
                throw new InventarioException(EnumResponse.CANTIDAD.value());
            }
            ClienteEntity cliente = clienteService.getClienteById(codCliente);
            if (Objects.isNull(cliente)) {
                message = EnumResponse.NO_CLIENTE_ID.value();
                throw new InventarioException(EnumResponse.NO_CLIENTE_ID.value());
            }

            VentaEntity venta = new VentaEntity();
            venta.setId(ventaService.getIdVenta());
            venta.setIdProducto(producto.getId());
            venta.setIdTienda(tienda.getId());
            venta.setPrecio(producto.getPrice());
            venta.setCantidad(cantidad);
            venta.setFecha(new Date());
            venta.setIdCliente(codCliente);
            ventaService.save(venta);
            producto.setStock(producto.getStock()-cantidad);
            productoService.getActualizarProducto(producto);
            transaccionService.save(codCliente,producto.getId(), cantidad, tienda.getId());

            respuesta.put(EnumResponse.VENTA.code(),EnumResponse.VENTA.value());
        } catch (Exception e) {
            respuesta.put(EnumResponse.ERROR.code(), message);
        }
        return ResponseEntity.ok().body(respuesta);
    }

}
