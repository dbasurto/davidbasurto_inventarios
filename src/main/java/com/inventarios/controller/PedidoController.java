package com.inventarios.controller;

import com.inventarios.Utils.Utils;
import com.inventarios.entity.ClienteEntity;
import com.inventarios.entity.PedidoEntity;
import com.inventarios.entity.ProductoEntity;
import com.inventarios.entity.TiendaEntity;
import com.inventarios.enums.EnumResponse;
import com.inventarios.exception.InventarioException;
import com.inventarios.model.ResponseModel;
import com.inventarios.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@RestController
@RequestMapping("/pedido")
public class PedidoController {

    @Autowired
    private ProductoService productoService;

    @Autowired
    private TiendaService tiendaService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ConsomeApiService consomeApiService;

    @Autowired
    private PedidoService pedidoService;

    @GetMapping(value = "/test")
    public ResponseEntity<ResponseModel> test() {
        var response = new ResponseModel(EnumResponse.OK.code());
        try {
            response.setMessage("Test...");
        } catch (Exception e) {
            response.setCode(EnumResponse.ERROR.code());
            response.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping(value = "/pedido")
    public ResponseEntity<Map<String, Object>> getPedido(@RequestParam("codTienda") String codTienda,
                                                         @RequestParam("codProducto") String codProducto,
                                                         @RequestParam("cantidad") Integer cantidad,
                                                         @RequestParam("codCliente") Integer codCliente) {
        HashMap<String, Object> respuesta = new HashMap<>();
        String message = "";
        try {
            TiendaEntity tienda = tiendaService.getTiendaByCodigo(codTienda);
            if (Objects.isNull(tienda)) {
                message = EnumResponse.NO_TIENDA.value();
                throw new InventarioException(EnumResponse.NO_TIENDA.value());
            }
            ClienteEntity cliente = clienteService.getClienteById(codCliente);
            if (Objects.isNull(cliente)) {
                message = EnumResponse.NO_CLIENTE_ID.value();
                throw new InventarioException(EnumResponse.NO_CLIENTE_ID.value());
            }
            if (cantidad <= 0) {
                message = EnumResponse.CANTIDAD.value();
                throw new InventarioException(EnumResponse.CANTIDAD.value());
            }
            ProductoEntity producto = productoService.getProductoCod(codProducto);
            if (Objects.isNull(producto)) {
                message = EnumResponse.NO_PRODUCTO.value();
                throw new InventarioException(EnumResponse.NO_PRODUCTO.value());
            }
            switch (Utils.stockFaltante(producto.getStock(), cantidad)) {
                case 1:
                    ProductoEntity productoEntity = cargar(codProducto);
                    respuesta.put("code",productoEntity.getCod());
                    respuesta.put("name",productoEntity.getName());
                    respuesta.put("stock",productoEntity.getStock());
                    throw new InventarioException(EnumResponse.UNIDADES_NO_DISPONIBLES.value());
                case 2:
                    message = EnumResponse.UNIDADES_NO_DISPONIBLES.value();
                    throw new InventarioException(EnumResponse.UNIDADES_NO_DISPONIBLES.value());
                case 0:
                    cargar(codProducto);
                    break;
            }

            PedidoEntity pedido = new PedidoEntity();
            pedido.setId(pedidoService.getIdPendido());
            pedido.setIdTienda(tienda.getId());
            pedido.setIdProducto(producto.getId());
            pedido.setIdCliente(codCliente);
            pedido.setCantidad(cantidad);
            pedidoService.save(pedido);
            producto.setStock(producto.getStock() - cantidad);
            productoService.getActualizarProducto(producto);

            respuesta.put(EnumResponse.PEDIDO.code(), EnumResponse.PEDIDO.value());
        } catch (Exception e) {
            respuesta.put(EnumResponse.ERROR.code(), message);
        }
        return ResponseEntity.ok().body(respuesta);
    }

    @Async
    ProductoEntity cargar(String codProducto) {
        return consomeApiService.cargarProductoEspecifico("https://mocki.io/v1/50dceff9-461a-4366-96a3-cf9f4983aad2", codProducto);
    }
}
