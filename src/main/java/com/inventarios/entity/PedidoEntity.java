package com.inventarios.entity;

import javax.persistence.*;
import java.util.Objects;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Entity
@Table(name = "pedido")
public class PedidoEntity {
    private Integer id;
    private Integer idCliente;
    private Integer idTienda;
    private Integer idProducto;
    private Integer cantidad;
    private ClienteEntity clienteByIdCliente;
    private TiendaEntity tiendaByIdTienda;
    private ProductoEntity productoByIdProducto;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "idCliente", nullable = true)
    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    @Basic
    @Column(name = "idTienda", nullable = true)
    public Integer getIdTienda() {
        return idTienda;
    }

    public void setIdTienda(Integer idTienda) {
        this.idTienda = idTienda;
    }

    @Basic
    @Column(name = "idProducto", nullable = true)
    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    @Basic
    @Column(name = "cantidad", nullable = true)
    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PedidoEntity that = (PedidoEntity) o;
        return id == that.id && Objects.equals(idCliente, that.idCliente) && Objects.equals(idTienda, that.idTienda) && Objects.equals(idProducto, that.idProducto) && Objects.equals(cantidad, that.cantidad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, idCliente, idTienda, idProducto, cantidad);
    }

    @Access(AccessType.PROPERTY)
    @ManyToOne
    @JoinColumn(name = "idCliente", referencedColumnName = "id", insertable = false, updatable = false)
    public ClienteEntity getClienteByIdCliente() {
        return clienteByIdCliente;
    }

    public void setClienteByIdCliente(ClienteEntity clienteByIdCliente) {
        this.clienteByIdCliente = clienteByIdCliente;
    }

    @Access(AccessType.PROPERTY)
    @ManyToOne
    @JoinColumn(name = "idTienda", referencedColumnName = "id", insertable = false, updatable = false)
    public TiendaEntity getTiendaByIdTienda() {
        return tiendaByIdTienda;
    }

    public void setTiendaByIdTienda(TiendaEntity tiendaByIdTienda) {
        this.tiendaByIdTienda = tiendaByIdTienda;
    }

    @Access(AccessType.PROPERTY)
    @ManyToOne
    @JoinColumn(name = "idProducto", referencedColumnName = "id", insertable = false, updatable = false)
    public ProductoEntity getProductoByIdProducto() {
        return productoByIdProducto;
    }

    public void setProductoByIdProducto(ProductoEntity productoByIdProducto) {
        this.productoByIdProducto = productoByIdProducto;
    }
}
