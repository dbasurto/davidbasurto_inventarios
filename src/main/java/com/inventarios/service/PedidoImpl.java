package com.inventarios.service;

import com.inventarios.entity.PedidoEntity;
import com.inventarios.repository.PedidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Service
public class PedidoImpl implements PedidoService {
    @Autowired
    private final PedidoRepository pedidoRepository;

    PedidoImpl(PedidoRepository pedidoRepository) {
        this.pedidoRepository = pedidoRepository;
    }

    @Override
    public Integer getIdPendido() {
        try {
            return pedidoRepository.getPendidoMax().get(0).getId() + 1;
        } catch (Exception e) {
            return 1;
        }
    }

    @Override
    public PedidoEntity save(PedidoEntity pedidoEntity) {
        try {
            return pedidoRepository.save(pedidoEntity);
        } catch (Exception e) {
            return null;
        }
    }
}
