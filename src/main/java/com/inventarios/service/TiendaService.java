package com.inventarios.service;

import com.inventarios.entity.TiendaEntity;
import org.springframework.stereotype.Service;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Service
public interface TiendaService {

    TiendaEntity getTiendaByCodigo(String codigo);

}
