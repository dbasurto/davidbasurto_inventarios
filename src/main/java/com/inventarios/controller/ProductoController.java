package com.inventarios.controller;

import com.inventarios.entity.ProductoEntity;
import com.inventarios.enums.EnumResponse;
import com.inventarios.exception.InventarioException;
import com.inventarios.model.ResponseModel;
import com.inventarios.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0
 * @autor David Basurto [Date: 07 ene. 2022]
 **/
@RestController
@RequestMapping("/producto")
public class ProductoController {

    @Autowired
    private ProductoService productoService;

    @GetMapping(value = "/test")
    public ResponseEntity<ResponseModel> test() {
        var response = new ResponseModel(EnumResponse.OK.code());
        try {
            response.setMessage("Test...");
        } catch (Exception e) {
            response.setCode(EnumResponse.ERROR.code());
            response.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/allProduct")
    public ResponseEntity<Map<String, Object>> producto() {
        HashMap<String, Object> respuesta = new HashMap<>();
        try {
            List<ProductoEntity> lstProducto = productoService.getProducto();
            if (!lstProducto.isEmpty()) {
                for (ProductoEntity p : lstProducto) {
                    respuesta.put("cod", p.getCod());
                    respuesta.put("name", p.getName());
                }
            }
        } catch (Exception e) {
            respuesta.put(EnumResponse.ERROR.code(), EnumResponse.ERROR.value());
        }
        return ResponseEntity.ok().body(respuesta);
    }

    @PostMapping(value = "/updateProduct")
    public ResponseEntity<Map<String, Object>> updateProducto(@RequestParam("codProduct") String codProduct,
                                                              @RequestParam("stock") Integer stock) {
        HashMap<String, Object> respuesta = new HashMap<>();
        String message = "";
        try {
            ProductoEntity producto = productoService.getProductoCod(codProduct);
            if (Objects.isNull(producto)) {
                message = EnumResponse.NO_PRODUCTO.value();
                throw new InventarioException(EnumResponse.NO_PRODUCTO.value());
            }
            if (stock <= 0) {
                message = EnumResponse.STOCK.value();
                throw new InventarioException(EnumResponse.STOCK.value());
            }
            producto.setStock(producto.getStock() + stock);
            productoService.getActualizarProducto(producto);
            respuesta.put(EnumResponse.ACTUALIZAR_PRODUCTO.code(), EnumResponse.ACTUALIZAR_PRODUCTO.value());

        } catch (Exception e) {
            respuesta.put(EnumResponse.ERROR.code(), message);
        }
        return ResponseEntity.ok().body(respuesta);
    }


}
