package com.inventarios.service;

import com.inventarios.entity.VentaEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Service
public interface VentaService {

    VentaEntity save(VentaEntity venta);

    Integer getIdVenta();

    List<Object[]> getReporte();

}
