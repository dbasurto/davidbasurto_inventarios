package com.inventarios.Utils;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
public class Utils {

    public static Integer stockFaltante(Integer stock, Integer pedido) {
        Integer total = (stock - pedido);
        if (total <= -10) {
            return 2;
        } else if ((total < -5 && total > -10)) {
            return 1;
        } else if ((total > -6 && total < 0)) {
            return 0;
        }
        return 3;
    }
}
