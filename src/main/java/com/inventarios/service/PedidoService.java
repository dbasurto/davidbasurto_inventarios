package com.inventarios.service;

import com.inventarios.entity.PedidoEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Service
public interface PedidoService {

    Integer getIdPendido();

    PedidoEntity save(PedidoEntity pedidoEntity);
}
