package com.inventarios.model;

import lombok.Data;

import java.io.Serializable;
/**
 * @version 1.0
 * @autor David Basurto [Date: 07 ene. 2022]
 **/

@Data
public class ResponseModel implements Serializable {

    private static final long serialVersionUID = 1L;

    String code;
    String message;

    public ResponseModel(String code) {
        this.code = code;
        this.message = "";
    }



}
