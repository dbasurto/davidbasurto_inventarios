package com.inventarios.service;

import com.inventarios.entity.TransaccionEntity;
import com.inventarios.repository.TransaccionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Service
public class TransaccionImpl implements TransaccionService {

    @Autowired
    private final TransaccionRepository transaccionRepository;

    TransaccionImpl(TransaccionRepository transaccionRepository) {
        this.transaccionRepository = transaccionRepository;
    }

    public Integer getIdTransaccion() {
        try {
            return transaccionRepository.getTransaccionMax().get(0).getId() + 1;
        } catch (Exception e) {
            return 1;
        }
    }

    @Override
    public TransaccionEntity save(Integer idCliente, Integer idProducto, Integer cantidad, Integer idTienda) {
        try {
            TransaccionEntity transaccionEntity = new TransaccionEntity();
            transaccionEntity.setId(getIdTransaccion());
            transaccionEntity.setIdCliente(idCliente);
            transaccionEntity.setIdProducto(idProducto);
            transaccionEntity.setCantidad(cantidad);
            transaccionEntity.setIdTienda(idTienda);
            transaccionEntity.setFecha(new Date());

            return transaccionRepository.save(transaccionEntity);
        } catch (Exception e) {
            return null;
        }
    }

    public List<Object[]> getReporte() {
        try {
            return transaccionRepository.getReporte();
        } catch (Exception e) {
            return null;
        }
    }

    public List<TransaccionEntity> getTransaccionFecha(Date fechaInicio, Date fechaFin) {
        try {
            return transaccionRepository.getTransaccionFecha(fechaInicio, fechaFin);
        } catch (Exception e) {
            return null;
        }
    }
}
