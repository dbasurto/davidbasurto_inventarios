package com.inventarios.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Objects;

/**
 * @version 1.0s
 * @autor David Basurto [Date: 07 ene. 2022]
 **/
@Entity
@Table(name = "producto")
public class ProductoEntity {
    @Id
    private Integer id;
    private String cod;
    private String name;
    private BigDecimal price;
    private Integer stock;
    private Collection<PedidoEntity> pedidosById;
    private Collection<VentaEntity> ventasById;

    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cod", nullable = true, length = 25)
    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "price", nullable = true)
    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Basic
    @Column(name = "stock", nullable = true)
    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductoEntity that = (ProductoEntity) o;
        return id == that.id && Objects.equals(cod, that.cod) && Objects.equals(name, that.name) && Objects.equals(price, that.price) && Objects.equals(stock, that.stock);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cod, name, price, stock);
    }

    @Access(AccessType.PROPERTY)
    @OneToMany(mappedBy = "productoByIdProducto")
    public Collection<PedidoEntity> getPedidosById() {
        return pedidosById;
    }

    public void setPedidosById(Collection<PedidoEntity> pedidosById) {
        this.pedidosById = pedidosById;
    }

    @Access(AccessType.PROPERTY)
    @OneToMany(mappedBy = "productoByIdProducto")
    public Collection<VentaEntity> getVentasById() {
        return ventasById;
    }

    public void setVentasById(Collection<VentaEntity> ventasById) {
        this.ventasById = ventasById;
    }

}
