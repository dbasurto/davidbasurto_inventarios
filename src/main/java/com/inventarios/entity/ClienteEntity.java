package com.inventarios.entity;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

/**
 * @version 1.0
 * @autor David Basurto [Date: 09 ene. 2022]
 **/
@Entity
@Table(name = "cliente")
public class ClienteEntity {
    private Integer id;
    private String identificacion;
    private String nombre;
    private Collection<PedidoEntity> pedidosById;
    private Collection<TransaccionEntity> transaccionsById;
    private Collection<VentaEntity> ventasById;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "identificacion", nullable = true, length = 15)
    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    @Basic
    @Column(name = "nombre", nullable = true, length = 50)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClienteEntity that = (ClienteEntity) o;
        return id == that.id && Objects.equals(identificacion, that.identificacion) && Objects.equals(nombre, that.nombre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, identificacion, nombre);
    }

    @Access(AccessType.PROPERTY)
    @OneToMany(mappedBy = "clienteByIdCliente")
    public Collection<PedidoEntity> getPedidosById() {
        return pedidosById;
    }

    public void setPedidosById(Collection<PedidoEntity> pedidosById) {
        this.pedidosById = pedidosById;
    }

    @Access(AccessType.PROPERTY)
    @OneToMany(mappedBy = "clienteByIdCliente")
    public Collection<TransaccionEntity> getTransaccionsById() {
        return transaccionsById;
    }

    public void setTransaccionsById(Collection<TransaccionEntity> transaccionsById) {
        this.transaccionsById = transaccionsById;
    }

    @Access(AccessType.PROPERTY)
    @OneToMany(mappedBy = "clienteByIdCliente")
    public Collection<VentaEntity> getVentasById() {
        return ventasById;
    }

    public void setVentasById(Collection<VentaEntity> ventasById) {
        this.ventasById = ventasById;
    }
}
