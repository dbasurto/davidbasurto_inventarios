package com.inventarios.controller;

import com.inventarios.entity.ClienteEntity;
import com.inventarios.enums.EnumResponse;
import com.inventarios.exception.InventarioException;
import com.inventarios.model.ResponseModel;
import com.inventarios.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @version 1.0
 * @autor David Basurto [Date: 08 ene. 2022]
 **/
@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping(value = "/test")
    public ResponseEntity<ResponseModel> test() {
        var response = new ResponseModel(EnumResponse.OK.code());
        try {
            response.setMessage("Test...");
        } catch (Exception e) {
            response.setCode(EnumResponse.ERROR.code());
            response.setMessage(e.getMessage());
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/allClient")
    public ResponseEntity<Map<String, Object>> getAllCliente() {
        HashMap<String, Object> respuesta = new HashMap<>();
        try {
            List<ClienteEntity> lstCliente = clienteService.getCliente();
            if (!lstCliente.isEmpty()) {
                for (ClienteEntity p : lstCliente) {
                    respuesta.put("id", p.getId());
                    respuesta.put("nombre", p.getNombre());
                    respuesta.put("identificacion", p.getIdentificacion());
                }
            }
        } catch (Exception e) {
            respuesta.put(EnumResponse.ERROR.code(), EnumResponse.ERROR.value());
        }
        return ResponseEntity.ok().body(respuesta);
    }

    @GetMapping(value = "/getClient/{identificacion}")
    public ResponseEntity<Map<String, Object>> getCliente(@PathVariable("identificacion") final String identificacion) {
        HashMap<String, Object> respuesta = new HashMap<>();
        try {
            ClienteEntity cliente = clienteService.getClienteByIdentificacion(identificacion);
            if(Objects.isNull(cliente)) {
                respuesta.put(EnumResponse.NO_CLIENTE.code(), EnumResponse.NO_CLIENTE.value());
            }else {
                respuesta.put("id", cliente.getId());
                respuesta.put("nombre", cliente.getNombre());
                respuesta.put("identificacion", cliente.getIdentificacion());
            }
        } catch (Exception e) {
            respuesta.put(EnumResponse.ERROR.code(), EnumResponse.ERROR.value());
        }
        return ResponseEntity.ok().body(respuesta);
    }

    @PostMapping(value = "/updateClient")
    public ResponseEntity<Map<String, Object>> udpCliente(@RequestParam("id") Integer id,
                                                          @RequestParam("identificación") String identificacion,
                                                          @RequestParam("nombre") String nombre) {
        HashMap<String, Object> respuesta = new HashMap<>();
        String message = "";
        try {
            ClienteEntity cliente = clienteService.getClienteById(id);
            if (Objects.isNull(cliente)) {
                message = EnumResponse.NO_CLIENTE_ID.value();
                throw new InventarioException(EnumResponse.NO_CLIENTE_ID.value());
            }
            cliente.setIdentificacion(identificacion);
            cliente.setNombre(nombre);
            clienteService.save(cliente);
            respuesta.put(EnumResponse.CLIENTE_UDP.code(), EnumResponse.CLIENTE_UDP.value());

        } catch (Exception e) {
            respuesta.put(EnumResponse.ERROR.code(), message);
        }
        return ResponseEntity.ok().body(respuesta);
    }

    @PostMapping(value = "/createClient")
    public ResponseEntity<Map<String, Object>> crtCliente(@RequestParam("identificación") String identificacion,
                                                          @RequestParam("nombre") String nombre) {
        HashMap<String, Object> respuesta = new HashMap<>();
        try {
            ClienteEntity cliente = new ClienteEntity();
            cliente.setId(clienteService.idCliente());
            cliente.setIdentificacion(identificacion);
            cliente.setNombre(nombre);
            clienteService.save(cliente);
            respuesta.put(EnumResponse.CLIENTE_CRT.code(), EnumResponse.CLIENTE_CRT.value());

        } catch (Exception e) {
            respuesta.put(EnumResponse.ERROR.code(), EnumResponse.ERROR.value());
        }
        return ResponseEntity.ok().body(respuesta);
    }

    @GetMapping(value = "/deleteClient/{id}")
    public ResponseEntity<Map<String, Object>> dltCliente(@PathVariable("id") Integer id) {
        HashMap<String, Object> respuesta = new HashMap<>();
        try {
            ClienteEntity cliente = clienteService.getClienteById(id);
            clienteService.delete(cliente);
            respuesta.put(EnumResponse.CLIENTE_DLT.code(), EnumResponse.CLIENTE_DLT.value());

        } catch (Exception e) {
            respuesta.put(EnumResponse.ERROR.code(), EnumResponse.ERROR.value());
        }
        return ResponseEntity.ok().body(respuesta);
    }

}
